# Web Worker Lab

The Web Worker Lab is intended to be a friendly application to perform epxeriments with
[Web Workers](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API).


## Contributing

The Web Worker Lab is published under the terms of the GNU GPLv3 so feel free to use, copy,
redistribute and/or improve it as you like !

(see [LICENSE](./LICENSE) for more details).

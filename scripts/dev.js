const esbuild = require('esbuild');
const sveltePlugin = require('esbuild-svelte');

esbuild
  .serve({
    servedir: './app',
    port: 8080
  }, {
    entryPoints: ['./app/main.js'],
    outdir: './app/dist',
    format: 'esm',
    bundle: true,
    sourcemap: true,
    plugins: [
      sveltePlugin()
    ]
  })
  .catch(err => {
    console.log(err);
    process.exit(1);
  });

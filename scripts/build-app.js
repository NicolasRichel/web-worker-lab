const fs = require('fs');
const esbuild = require('esbuild');
const sveltePlugin = require('esbuild-svelte');

esbuild
  .buildSync({
    entryPoints: ['./app/main.js'],
    outdir: './dist',
    format: 'esm',
    bundle: true,
    plugins: [
      sveltePlugin()
    ]
  })
  .catch(
    () => process.exit(1)
  );

fs.copyFile(
  './app/index.html',
  './dist/index.html',
  err => err && console.log(err)
);
fs.copyFile(
  './app/favicon.png',
  './dist/favicon.png',
  err => err && console.log(err)
);

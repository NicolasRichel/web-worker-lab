export default class ExtensibleWorker {

  constructor(src, extension, options) {
    this.worker = new Worker("./worker-extension.js", options);
    this.worker.postMessage({ cmd: "init", args: [ extension, src ] });
  }

}

self.onmessage = (e) => {
  if (e.data.cmd === "init") {
    self.importScripts(...e.data.args);
  }
};

export default class PromiseWorker {

  constructor(src) {
    this.pending = new Map();
    this.worker = new Worker("./worker-extension.js");
    this.worker.postMessage({ cmd: "init", args: [ src ] });
    this._setMessageHandler();
    this._setErrorHandler();
  }

  postMessage(data) {
    const id = crypto.randomUUID();
    return new Promise((resolve, reject) => {
      this.pending.set(id, { resolve, reject });
      this.worker.postMessage({ id, data });
    });
  }

  _setMessageHandler() {
    this.worker.onmessage = (e) => {
      const { id, data } = e.data;
      const resolver = this.pending.get(id);
      if (resolver) {
        resolver.resolve(data);
        this.pending.delete(id);
      }
    };
  }

  _setErrorHandler() {
    this.worker.onerror = (e) => {
      const { id, error } = JSON.parse(e.message.match(/{(.*)}/g)[0]);
      const resolver = this.pending.get(id);
      if (resolver) {
        resolver.reject(error);
        this.pending.delete(id);
      }
    };
  }

}

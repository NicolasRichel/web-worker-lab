self.sendData = (e, data) => {
  self.postMessage({ id: e.data.id, data });
};

self.sendError = (e, error) => {
  throw new Error(JSON.stringify({
    id: e.data.id,
    error
  }));
};

self.onmessage = (e) => {
  if (e.data.cmd === "init") {
    self.importScripts(e.data.args[0]);
  }
};

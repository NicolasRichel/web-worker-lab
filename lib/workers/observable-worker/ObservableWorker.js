export default class ObservableWorker {

  constructor(src) {
    this.subscriptions = new Map();
    this.worker = new Worker(src);
    this._setMessageHandler();
    this._setErrorHandler();
  }

  subscribe(onMessage, onError) {
    const subID = crypto.randomUUID();
    this.subscriptions.set(subID, { onMessage, onError });
    return subID;
  }

  unsubscribe(id) {
    return this.subscriptions.delete(id);
  }

  _setMessageHandler() {
    this.worker.onmessage = (e) => {
      this.subscriptions.forEach(
        sub => sub.onMessage(e.data)
      );
    };
  }

  _setErrorHandler() {
    this.worker.onerror = (e) => {
      this.subscriptions.forEach(
        sub => sub.onError(
          JSON.parse(e.message.match(/{(.*)}/g)[0])
        )
      );
    };
  }

}

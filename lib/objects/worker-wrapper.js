/**
 * Worker Wrapper
 */
class WorkerWrapper {
  
  constructor(worker) {
    this.worker = worker;
  }

  use(extension) {
    // TODO
  }

  post(data) {
    // TODO
  }

  on(event, callback) {
    // TODO
  }

}

export default WorkerWrapper;

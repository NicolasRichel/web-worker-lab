import WorkerWrapper from "./worker-wrapper.js";

/**
 * Worker Factory
 */
class WorkerFactory {

  constructor(config) {
    this.config = config;
  }

  create(src) {
    const worker = new Worker(src);
    return new WorkerWrapper(worker);
  }

}

export default WorkerFactory;

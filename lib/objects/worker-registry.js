/**
 * Worker Registry
 */
class WorkerRegistry {
  
  constructor() {
    this._registry = new Map();
    this._listeners = {
      "worker-added":   new Map(),
      "worker-removed": new Map(),
      "clear":          new Map()
    };
  }

  on(event, callback) {
    if (this._listeners[event]) {
      const subID = crypto.randomUUID();
      this._listeners[event].set(subID, callback);
      return subID;
    }
    console.warn(`[WorkerRegistry] - event '${event}' does not exist, unable to subscribe.`);
    return false;
  }

  off(subID) {
    return Object.values(this._listeners)
      .filter(callbackMap => callbackMap.has(subID))
      [0]?.delete(subID);
  }

  dispatch(event, data) {
    if (this._listeners[event]) {
      this._listeners[event].forEach(callback => callback(data));
    }
    console.warn(`[WorkerRegistry] - event '${event}' does not exist, unable to dispatch.`);
    return false;
  }

  getWorker(uuid) {
    return this._registry.get(uuid);
  }

  addWorker(worker) {
    const uuid = crypto.randomUUID();
    this._registry.set(uuid, worker);
    this.dispatch("worker-added", { uuid, worker });
    return uuid;
  }

  removeWorker(uuid) {
    if (this._registry.has(uuid)) {
      const worker = this._registry.get(uuid);
      this._registry.delete(uuid);
      this.dispatch("worker-removed", { uuid, worker });
      return worker;
    }
    return false;
  }

  clear() {
    this._registry.clear();
    this.dispatch("clear");
  }

}

export default WorkerRegistry;

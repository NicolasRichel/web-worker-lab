import { createShadowDOM, reflectBooleanAttribute } from "@nrl/web-component-utils";

/**
 * Worker Button
 *
 * The worker button is a button element that create a new Web Worker when clicked.
 * The worker script must be specified with a 'src' attribute.
 * If no 'src' attribute is provided the button will be disabled.
 * 
 * Attributes:
 *  - disabled : whether this worker button is disabled or not
 *  - name : name of the created workers
 *  - src : worker source script
 * 
 * Events:
 *  - 'worker-created' : fired when a worker is created
 *      details: { uuid, name, worker }
 */

const ELEMENT_NAME = "worker-button";

const template = document.createElement("template");
template.innerHTML = `
<style>
:host {
  box-sizing: border-box;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: 36px;
  height: 36px;
  border-radius: 50%;
  box-shadow: 0 1px 5px 1px rgba(0, 0, 0, 0.3);
  background-color: blue;
  cursor: pointer;
  transition: transform 0.3s ease-out;
}

:host::before {
  content: " ";
  width: 50%;
  height: 50%;
  background-color: white;
  clip-path: polygon(0 40%, 40% 40%, 40% 0, 60% 0, 60% 40%, 100% 40%, 100% 60%, 60% 60%, 60% 100%, 40% 100%, 40% 60%, 0 60%);
}

:host(:hover) {
  transform: scale(1.02);
}
</style>
`;

class WorkerButton extends HTMLElement {

  constructor() {
    super();
    createShadowDOM(this, template);
    reflectBooleanAttribute(this, "disabled");
  }

  connectedCallback() {
    this.addEventListener("click", () => this.createWorker());
    if (!window.Worker) {
      console.warn("[WorkerButton] - Web Workers not supported.");
      this.disabled = true;
    }
    if (!this.hasAttribute("src")) {
      console.warn("[WorkerButton] - Missing mandatory 'src' attribute.");
      this.disabled = true;
    }
  }

  createWorker() {
    if (!this.disabled) {
      const worker = new Worker(this.getAttribute("src"));
      const event = new CustomEvent("worker-created", {
        detail: {
          uuid: crypto.randomUUID(),
          name: this.hasAttribute('name') || undefined,
          worker: worker
        }
      });
      this.dispatchEvent(event);
    }
  }

}

window.customElements.define(ELEMENT_NAME, WorkerButton);

export default WorkerButton;

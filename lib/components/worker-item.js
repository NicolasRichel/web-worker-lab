import { createShadowDOM } from "@nrl/web-component-utils";

const states = Object.freeze({
  IDLE: "IDLE",
  RUNNING: "RUNNING",
  STOPPED: "STOPPED"
});

/**
 * Worker Item
 *
 * A worker item provide an interface to interact with a running worker.
 * 
 * Attributes:
 *  - uuid: (Required) the worker uuid
 *  - registry: the registry in which to look for worker
 *  - name: an optional worker name
 */

const ELEMENT_NAME = 'worker-item';

const template = document.createElement('template');
template.innerHTML = `
<div>
  <label>UUID :</label>
  <span data-uuid></span>
</div>
<div>
  <label>Name :</label>
  <span data-name></span>
</div>
<div>
  <label>State :</label>
  <span data-state></span>
</div>
<div>
  <button class="btn-stop">Stop</button>
</div>
<style>
:host {
  box-sizing: border-box;
  position: relative;
}

label {
  font-weight: bold;
}
</style>
`;

class WorkerItem extends HTMLElement {

  constructor() {
    super();
    this.registry = null;
    this.worker = null;
    this.state = "";
    createShadowDOM(this, template);
  }

  connectedCallback() {
    if (!this.hasAttribute("uuid")) {
      throw new Error("[WorkerItem] - Missing required attribute 'uuid'.");
    }
    const registryName = this.hasAttribute("registry")
      ? this.getAttribute("registry")
      : "default";
    this.registry = window.workerRegistries[registryName];
    if (this.registry) {
      this.worker = this.registry.getWorker(this.getAttribute("uuid"));
    } else {
      throw new Error(`[WorkerItem] - No registry found with name '${registryName}}'.`);
    }
    this.shadowRoot
      .querySelector("[data-uuid]")
      .textContent = this.getAttribute("uuid");
    this.shadowRoot
      .querySelector("[data-name]")
      .textContent = this.getAttribute("name");
    this.state = states.RUNNING;
    this.shadowRoot
      .querySelector("[data-state]")
      .textContent = states.RUNNING;
    this.shadowRoot
      .querySelector(".btn-stop")
      .addEventListener("click", () => this.stopWorker());
  }

  stopWorker() {
    this.worker.terminate();
    this.state = states.STOPPED;
    this.shadowRoot
      .getElementById("state")
      .textContent = states.STOPPED;
  }

}

window.customElements.define(ELEMENT_NAME, WorkerItem);

export default WorkerItem;

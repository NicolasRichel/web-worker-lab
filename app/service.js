import WorkerFactory from "../lib/objects/worker-factory.js";
import WorkerRegistry from "../lib/objects/worker-registry.js";

const factory = new WorkerFactory();
const registry = new WorkerRegistry();

function createWorker() {
  const worker = factory.create("/worker.js");
  registry.addWorker(worker);
}

export {
  registry,
  createWorker
};
